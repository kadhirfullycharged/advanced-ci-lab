from django.urls import path

from . import views

urlpatterns = [
    path(
        '',
        views.SinglePageView.as_view(),
        name='single_page',
    ),
    path(
        'error_simulation/',
        views.ErrorView.as_view(),
        name='error'
    )
]
